#ifndef STACKNODE_H
#define STACKNODE_H

#include "StackableObject.h"

class StackNode
{
    public:
        StackNode(StackableObject* objeto);
        StackNode* getPrevNode();
        void setPrevNode(StackNode* nodo);
        StackableObject* getObject();
    private:
        StackableObject* objeto;
        StackNode* NodoPrevio;

};

#endif // STACKNODE_H