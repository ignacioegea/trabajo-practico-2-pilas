#include <iostream>
#include "StackNode.h"

StackNode::StackNode(StackableObject * objecto) //Constructor
{
    this->objeto = objecto;
    this->NodoPrevio = NULL;
}

StackableObject* StackNode::getObject() //Devuelvo el objeto del nodo
{
    return this->objeto;
}

StackNode* StackNode::getPrevNode() //Devuelvo el nodo previo
{
    return this->NodoPrevio;
}

void StackNode::setPrevNode(StackNode* nodo) //Seteo el nodo previo
{
    this->NodoPrevio = nodo;
}
