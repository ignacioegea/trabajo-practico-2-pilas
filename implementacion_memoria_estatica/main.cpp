#include "StackInterface.h"
#include "FixedArrayStack.h"
#include "StackUser.h"

int main()
{
    FixedArrayStack pila;
    StackUser usuario(&pila);
    usuario.cargarPila();
    usuario.imprimirPila();
    return 0;
}
