#ifndef VEHICULO_H
#define VEHICULO_H

#include "StackableObject.h"
class Vehiculo : public StackableObject
{
    private:
        int cantRuedas;
        bool tieneBaul;
        float cilindrada;

    public:
        Vehiculo(int ruedas, bool baul, float cc);
        void mostrarEnPantalla();

        int GetCantRuedas();
        bool GetTieneBaul();
        float GetCilindrada();

        void SetCantRuedas(int ruedas);
        void SetTieneBaul(bool baul);
        void SetCilindrada(float cc);

};

#endif // VEHICULO_H