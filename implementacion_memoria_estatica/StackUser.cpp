#include <iostream>
#include "StackUser.h"
#include "Vehiculo.h"

using namespace std;

StackUser::StackUser(StackInterface * pila) // Constructor
{
    this->stack=pila;
}

void StackUser::cargarPila() //Le cargamos 2 vehiculos
{  
    StackableObject* objeto = new Vehiculo(4, true, 1.6);
    this->stack->push(objeto);
    objeto=new Vehiculo(2, false, 1.0);
    this->stack->push(objeto);
}

void StackUser::imprimirPila() //Imprime toda la pila
{
    StackableObject* aux;
    while(this->stack->getCount()!=0)
    {
        aux=this->stack->pop();
        aux->mostrarEnPantalla();
    }
}