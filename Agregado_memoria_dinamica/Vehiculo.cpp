#include <iostream>
#include "Vehiculo.h"

using namespace std;

void Vehiculo::mostrarEnPantalla() //Uso los metodos getter para mostrar en pantalla los atributos
{

    cout<<"Este vehiculo tiene:"<<endl;
    cout<<"Cilindrada: "<<this->GetCilindrada()<<endl;
    cout<<"Ruedas: "<<this->GetCantRuedas()<<endl;
    cout<<"Baul: ";
    if(this->GetTieneBaul())
        cout<<"si"<<endl;
    else
        cout<<"no"<<endl;
    cout<<endl;
}

Vehiculo::Vehiculo(int ruedas, bool baul, float cc) //Uso los metodos setter para asignar valores a los atributos
{

    this->SetCantRuedas(ruedas);
    this->SetCilindrada(cc);
    this->SetTieneBaul(baul);
}

int Vehiculo::GetCantRuedas()
{
    return(this->cantRuedas);
}

float Vehiculo::GetCilindrada()
{
    return(this->cilindrada);
}

bool Vehiculo::GetTieneBaul()
{
    return(this->tieneBaul);
}

void Vehiculo::SetCantRuedas(int ruedas)
{
    this->cantRuedas=ruedas;
}

void Vehiculo::SetCilindrada(float cc)
{
    this->cilindrada=cc;
}

void Vehiculo::SetTieneBaul(bool baul)
{
    this->tieneBaul=baul;
}

