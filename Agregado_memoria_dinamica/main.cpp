#include "StackInterface.h"
#include "DynamicMemStack.h"
#include "FixedArrayStack.h"
#include "StackUser.h"

int main()
{
    DynamicMemStack pila1;
    FixedArrayStack pila2;
    StackUser usuario1(&pila1);
    StackUser usuario2(&pila2);
    usuario1.cargarPila();
    usuario1.imprimirPila();
    usuario2.cargarPila();
    usuario2.imprimirPila();
    return 0;
}
